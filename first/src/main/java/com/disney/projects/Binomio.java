package com.disney.projects;

import java.util.Scanner;

public class Binomio {
	// a(2)+2ab+b(2)

	public static void pow(int a, int b) {

		double result = Math.pow(a, 2) + (2 * a * b) + Math.pow(b, 2);
		System.out.println("result pow" + "  " + result);
	}

	public static void square(int a, int b) {
		double resulta = Math.sqrt(a);
		double resultb = Math.sqrt(b);

		System.out.println("result square root " + "  " + resulta);
		System.out.println("result square root " + "  " + resultb);

	}

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter value a");
		int a = scanner.nextInt();
		System.out.println("Enter value b");
		int b = scanner.nextInt();

		pow(a, b);
		square(a, b);

		scanner.close();

	}
}
