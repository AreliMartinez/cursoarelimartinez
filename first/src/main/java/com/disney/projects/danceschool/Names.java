package com.disney.projects.danceschool;

import com.disney.projects.interfaces.Name;

public class Names implements Name {

	String name;
	int age;

	public Names() {

	}

	public Names(String name, int age) {
		this.name = name;
		this.age = age;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;

	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void returnName() {
		System.out.println("Good Name");

	}

	public String toString() {
		return "Name [Name=" + name + ", Age=" + age + "]";
	}

	public boolean equals(Object obj) {

		if (obj != null && obj instanceof Names) {
			Names other = ((Names) obj);
			return other.name.equals(this.name) && other.age == this.age;
		}
		return false;
	}

}
