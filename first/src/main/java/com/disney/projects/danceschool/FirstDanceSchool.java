package com.disney.projects.danceschool;

public class FirstDanceSchool extends DanceSchool {

	public FirstDanceSchool(String name, String musical_genre, double cost) {
		super(name, musical_genre, cost);
	}

	public void returnMusicalGenre() {
		System.out.println("The gender is :" + getMusical_genre());

	}

	public String toString() {
		return "Dance_School [Name=" + name + ", genre=" + musical_genre + ", costo=" + cost + "]";
	}

}
