package com.disney.projects.danceschool;

import com.disney.projects.interfaces.Gender;

public class DanceSchool implements Gender {

	String name;
	String musical_genre;
	double cost;

	public DanceSchool() {

	}

	public DanceSchool(String name, String musical_genre, double cost) {
		this.name = name;
		this.musical_genre = musical_genre;
		this.cost = cost;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;

	}

	public String getMusical_genre() {
		return musical_genre;
	}

	public void setMusical_genre(String musical_genre) {
		this.musical_genre = musical_genre;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public void  returnMusicalGenre() {
		System.out.println("Dance School");

	}
	
	
	public String toString() {
		return "Dance_School [Name=" + name + ", genre=" + musical_genre + ", cost=" + cost + "]";
	}

	
	public boolean equals(Object obj) {

		if (obj != null && obj instanceof DanceSchool) {
			DanceSchool other = ((DanceSchool) obj);
			return other.name == this.name && other.musical_genre == this.musical_genre && other.cost == this.cost;
		}
		return false;
	}

}
