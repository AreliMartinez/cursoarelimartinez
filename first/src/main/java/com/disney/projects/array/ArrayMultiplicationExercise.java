package com.disney.projects.array;

public class ArrayMultiplicationExercise {

	private int table;
	private int stop;

	public ArrayMultiplicationExercise() {

	}

	public int getTable() {
		return table;

	}

	public void setTable(int table) {
		this.table = table;

	}

	public int getStop() {
		return stop;
	}

	public void setStop(int stop) {
		this.stop = stop;
	}

	public void multiplicationTable(int[] arraytable) {

		for (int number = 1; number <= arraytable.length; number++) {
			arraytable[number - 1] = stop * number;
			System.out.println(table + "x" + number + "=" + table * number);

		}

	}
}
