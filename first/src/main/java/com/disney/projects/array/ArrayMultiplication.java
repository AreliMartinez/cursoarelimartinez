package com.disney.projects.array;

import java.util.Scanner;

public class ArrayMultiplication {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		ArrayMultiplicationExercise arrayMultiplicationExercise = new ArrayMultiplicationExercise();

		System.out.println("Enter number of the table multiplication");
		int table = scanner.nextInt();
		System.out.println("Enter the limit number of the table");
		int stop = scanner.nextInt();

		int[] arraytable = new int[stop];

		arrayMultiplicationExercise.setTable(table);
		arrayMultiplicationExercise.setStop(stop);
		arrayMultiplicationExercise.multiplicationTable(arraytable);

		scanner.close();

	}

}
