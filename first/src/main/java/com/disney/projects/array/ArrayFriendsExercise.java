package com.disney.projects.array;

public class ArrayFriendsExercise {

	private int friends;
	private String name;

	public ArrayFriendsExercise() {

	}

	public int getFriends() {
		return friends;
	}

	public void setFriends(int friends) {
		this.friends = friends;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void friendsMenu(String[] arrayFriends) {

		for (String name : arrayFriends) {
			System.out.println(name);

		}

	}
}
