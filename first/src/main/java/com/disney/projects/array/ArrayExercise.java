package com.disney.projects.array;

public class ArrayExercise {

	public static void main(String[] args) {

		String[] arraycolors = new String[5];// new espacio de memoria

		arraycolors[0] = "azul";
		arraycolors[1] = "rosa";
		arraycolors[2] = "morado";
		arraycolors[3] = "verde";
		arraycolors[4] = "amarillo";

		System.out.println(arraycolors[4]);

		for (int index = 0; index < arraycolors.length; index++) {
			System.out.println("position" + " " + index + " " + "values" + " " + arraycolors[index]);

		}
// arrayFriends[index] = name;// index va incrementando
		for (String index : arraycolors) {

			System.out.println(index);
		}

		String[] arrayAnonymous = { "azul", "morado", "verde", "violeta", "naranja" };

		System.out.println(arrayAnonymous.length);

		for (int index = 0; index < arrayAnonymous.length; index++) {
			System.out.println("position" + " " + index + " " + "values" + " " + arraycolors[index]);

		}
	}

}
