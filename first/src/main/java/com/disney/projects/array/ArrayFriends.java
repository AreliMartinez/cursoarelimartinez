package com.disney.projects.array;

import java.util.Scanner;

public class ArrayFriends {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		ArrayFriendsExercise arrayFriendsExercise = new ArrayFriendsExercise();
		String repeat = "";

		do {

			System.out.println("How many friends do you have");
			int friends = scanner.nextInt();
			arrayFriendsExercise.setFriends(friends);

			String[] arrayFriends = new String[arrayFriendsExercise.getFriends()];

			for (int index = 0; index < arrayFriends.length; index++) {
				System.out.println("Whats your name");
				String name = scanner.next();

				arrayFriendsExercise.setName(name);
				arrayFriends[index] = arrayFriendsExercise.getName();

			}
			arrayFriendsExercise.friendsMenu(arrayFriends);

			System.out.println("Repeat menu");
			repeat = scanner.next();
		} while (repeat.equalsIgnoreCase("Y"));
		scanner.close();

	}

}
