package com.disney.projects.map;

import java.util.Map;
import java.util.HashMap;
import java.util.Scanner;

public class MapInvoke {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		String repeat = "no";

		Map<Integer, String> map = new HashMap<Integer, String>();
		map.put(1, "Cardiologist");
		map.put(2, "Neurologist");
		map.put(3, "Dermatology");
		do {

			System.out.println("What do you want to do?");
			System.out.println("1.-Add Speciality  2.-Remove Speciality  3.-Search Speciality");
			int option = scanner.nextInt();

			switch (option) {
			case 1:
				System.out.println(map.values());

				System.out.println("Speciality key");
				int key = scanner.nextInt();
				System.out.println("Name of the Speciality?");
				String name = scanner.next();

				map.put(key, name); // put es para agregar

				System.out.println("::::::::::::::::::::");
				System.out.println(map);

				break;

			case 2:

				System.out.println(map.values());// values : muestra la lista
				// SetKey : muestra todos los indices

				System.out.println("Speciality key");
				key = scanner.nextInt();

				map.remove(key);

				System.out.println("::::::::::::::::::::");
				System.out.println(map);

				break;

			case 3:

				System.out.println("::::::::::::::::::::");
				System.out.println(map.values());

				break;

			default:
				System.out.println("Does not exist");
				break;

			}

			System.out.println("You want to repeat the menu? yes/no");
			repeat = scanner.next();
		} while (repeat.equalsIgnoreCase("yes"));

		scanner.close();
	}

}
