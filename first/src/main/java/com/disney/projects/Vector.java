package com.disney.projects;

import java.util.Scanner;

public class Vector {

	public static void vector(int x1, int x2, int y1, int y2, int z1, int z2) {
		double result = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2) + Math.pow((z2 - z1), 2));

		System.out.println("Module of a vector" + " " + result);

	}

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter side x2");
		int x1 = scanner.nextInt();
		System.out.println("Enter side x1");
		int x2 = scanner.nextInt();
		System.out.println("Enter side y2");
		int y1 = scanner.nextInt();
		System.out.println("Enter side y1");
		int y2 = scanner.nextInt();
		System.out.println("Enter side z2");
		int z1 = scanner.nextInt();
		System.out.println("Enter side z1");
		int z2 = scanner.nextInt();
		vector(x1, x2, y1, y2, z1, z2);

		scanner.close();

	}

}
