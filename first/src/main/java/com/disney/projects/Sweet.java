package com.disney.projects;

import java.util.Scanner;
//switch

public class Sweet {

	public static void sweet(int dessert) {
		switch (dessert) {

		case 1:
			System.out.println("Cost $250");
			break;
		case 2:
			System.out.println("Cost $180");
			break;
		case 3:
			System.out.println("Cost $130");
			break;
		case 4:
			System.out.println("Cost $85");
			break;
		default:
			System.out.println("There is no such option");
			break;

		}
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Choose a dessert");
		System.out.println("1.Cake");
		System.out.println("2.Jelly");
		System.out.println("3.Rice pudding");
		System.out.println("4.Cookies");
		int dessert = scanner.nextInt();
		sweet(dessert);
		scanner.close();
	}

}