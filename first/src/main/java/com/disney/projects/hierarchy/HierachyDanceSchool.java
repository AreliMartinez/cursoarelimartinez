package com.disney.projects.hierarchy;

import java.util.List;

import com.disney.projects.danceschool.DanceSchool;
import com.disney.projects.danceschool.FirstDanceSchool;
import com.disney.projects.danceschool.SecondDanceSchool;

import java.util.ArrayList;

public class HierachyDanceSchool {

	public static void main(String[] args) {

		DanceSchool dance = new DanceSchool("Tumbao Dance Studio", "Bachata / Salsa", 2500);
		dance.returnMusicalGenre();
		System.out.println(dance.getName());
		System.out.println(dance.getMusical_genre());
		System.out.println(dance.getCost());

		System.out.println("-----------------------------------");

		FirstDanceSchool firstschool = new FirstDanceSchool("Inteligente Estudio", "HIP HOP", 7500);

		firstschool.returnMusicalGenre();
		System.out.println(firstschool.getName());
		System.out.println(firstschool.getMusical_genre());
		System.out.println(firstschool.getCost());
		System.out.println("-----------------------------------");

		SecondDanceSchool secondschool = new SecondDanceSchool("Salsa Candela", "Salsa", 3700);

		secondschool.returnMusicalGenre();
		System.out.println(secondschool.getName());
		System.out.println(secondschool.getMusical_genre());
		System.out.println(secondschool.getCost());

		System.out.println("-----------------------------------");

		System.out.println(dance);
		System.out.println(firstschool);
		System.out.println(secondschool);

		List<DanceSchool> dances = new ArrayList<DanceSchool>();
		dances.add(dance);
		dances.add(firstschool);
		dances.add(secondschool);

	}

}
