package com.disney.projects.hierarchy;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AddRemove {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		String repeat = "no";

		List<String> names = new ArrayList<String>();
		names.add("Areli");
		names.add("Salvador");
		names.add("Fernando");

		do {
			System.out.println(names);

			System.out.println("1.- Add name");
			System.out.println("2.- Remove name");
			int option = scanner.nextInt();

			switch (option) {
			case 1:
				System.out.println("Enter a name");
				String name = scanner.next();
				names.add(name);

				System.out.println(names);

				break;

			case 2:
				System.out.println("What name do you want to remove");
				name = scanner.next();

				names.remove(name);

				System.out.println(names);

				break;

			default:
				break;
			}

			System.out.println("You want to repeat the menu? yes/no");
			repeat = scanner.next();

		} while (repeat.equalsIgnoreCase("yes"));

		System.out.println("Final List = " + names);

		scanner.close();
	}

}
