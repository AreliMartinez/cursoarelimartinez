package com.disney.projects.hierarchy;

import com.disney.projects.figures.Parallelogram;
import com.disney.projects.figures.Rectangle;

public class FiguresHierarchy {

	public static void main(String[] args) {

		Rectangle rectangle = new Rectangle(10.8, 29.0);

		rectangle.calculateArea();
		rectangle.calculatePerimeter();

		System.out.println("The area of ​​the rectangle is = " + rectangle.getArea());
		System.out.println("The perimeter of the rectangle is = " + rectangle.getPerimeter());

		Parallelogram parallelogram = new Parallelogram(3.5, 7.8);

		parallelogram.calculateArea();
		parallelogram.calculatePerimeter();

		System.out.println("The area of ​​the parallelogram is =" + parallelogram.getArea());
		System.out.println("The perimeter of the parallelogram is = " + parallelogram.getPerimeter());

	}

}