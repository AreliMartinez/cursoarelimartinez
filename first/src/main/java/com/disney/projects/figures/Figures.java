package com.disney.projects.figures;

public class Figures {

	private double area;
	private double perimeter;

	public Figures() {

	}

	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
	}

	public double getPerimeter() {
		return perimeter;

	}

	public void setPerimeter(double perimeter) {
		this.perimeter = perimeter;
	}

}
