package com.disney.projects.figures;

import com.disney.projects.interfaces.Figurables;

public class Parallelogram extends Figures implements Figurables {

	private double base;
	private double height;
	private static final int VALUE = 2;

	public Parallelogram() {

	}

	public Parallelogram(double base, double height) {
		this.base = base;
		this.height = height;
	}

	// base * height
	public void calculateArea() {
		this.setArea(base * height);
	}

	// 2* (base + height)
	public void calculatePerimeter() {
		this.setPerimeter(VALUE * (base + height));
	}

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}
}
