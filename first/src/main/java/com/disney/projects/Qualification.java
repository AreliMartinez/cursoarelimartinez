package com.disney.projects;

import java.util.Scanner;

//if-else
public class Qualification {

	public static void qualification(double one, double two, double three) {
		double result = (one + two + three) / 3;
		if (result >= 0 && result < 8) {
			System.out.println("Insufficient Note");
		} else if (result >= 8 && result < 9) {
			System.out.println("Good Note ");

		} else if (result >= 9 && result <= 10) {
			System.out.println("Excellent Note ");

		} else {

			System.out.println("Sorry, data is not valid");
		}
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter note one");
		double one = scanner.nextDouble();
		System.out.println("Enter note two");
		double two = scanner.nextDouble();
		System.out.println("Enter note three");
		double three = scanner.nextDouble();
		qualification(one, two, three);

		scanner.close();
	}

}
