package com.disney.projects.loops;

public class ExerciseTable {

	private int table;
	private int stop;

	public ExerciseTable() {

	}

	public int getTable() {
		return table;
	}

	public void setTable(int table) {
		this.table = table;
	}

	public int getStop() {
		return stop;
	}

	public void setStop(int stop) {
		this.stop = stop;
	}

	public void multiplicationTable() {

		int count = 1;
		while (count <= getStop()) {
			int result = count * getTable();
			System.out.println(getTable() + "x" + count + "=" + result);

			count++;

		}

	}
}
