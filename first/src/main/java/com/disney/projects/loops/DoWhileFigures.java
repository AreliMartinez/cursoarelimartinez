package com.disney.projects.loops;

import java.util.Scanner;

public class DoWhileFigures {

	public static void main(String[] ags) {

		DWExerciseFigures dwExerciseFigures = new DWExerciseFigures();

		Scanner scanner = new Scanner(System.in);
		String answer = "";

		do {

			System.out.println("Enter base");
			double base = scanner.nextDouble();
			System.out.println("Enter height");
			double height = scanner.nextDouble();

			System.out.println("Welcome ,Choose an option");
			System.out.println("1.Rectangle");
			System.out.println("2.Parallelogram");
			System.out.println("3.Kite");
			int option = scanner.nextInt();

			dwExerciseFigures.setBase(base);
			dwExerciseFigures.setHeight(height);
			dwExerciseFigures.setOption(option);
			dwExerciseFigures.setAnswer(answer);
			dwExerciseFigures.figuresGeometric();

			System.out.println("Want to repeat the menu Y/N");
			answer = scanner.next();

		} while (answer.equalsIgnoreCase("Y"));

		scanner.close();
	}

}