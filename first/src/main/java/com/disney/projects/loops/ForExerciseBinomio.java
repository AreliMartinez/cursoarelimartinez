package com.disney.projects.loops;

public class ForExerciseBinomio {
	private int a;
	private int b;
	private double result;

	public ForExerciseBinomio() {

	}

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}

	public double getResult() {
		return result;
	}

	public void setResult(double result) {
		this.result = result;
	}

	public void binomio() {
		result = Math.pow(a, 2) + (2 * a * b) + Math.pow(b, 2);
		System.out.println("result pow" + "  " + result);

	}

}
