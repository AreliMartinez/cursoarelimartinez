package com.disney.projects.loops;

public class WhileExerciseQualification {
	private double one;
	private double two;
	private double three;
	private double result;

	public WhileExerciseQualification() {

	}

	public double getOne() {
		return one;
	}

	public void setOne(double one) {
		this.one = one;
	}

	public double getTwo() {
		return two;
	}

	public void setTwo(double two) {
		this.two = two;
	}

	public double getThree() {
		return three;
	}

	public void setThree(double three) {
		this.three = three;
	}

	public double getResult() {
		return result;
	}

	public void setResult(double result) {
		this.result = result;
	}

	public void qualification() {
		result = (one + two + three) / 3;
		if (getResult() >= 0 && getResult() < 8) {
			System.out.println("Insufficient Note");
		} else if (getResult() >= 8 && getResult() < 9) {
			System.out.println("Good Note ");

		} else if (getResult() >= 9 && getResult() <= 10) {
			System.out.println("Excellent Note ");

		} else {

			System.out.println("Sorry, data is not valid");
		}
	}
}
