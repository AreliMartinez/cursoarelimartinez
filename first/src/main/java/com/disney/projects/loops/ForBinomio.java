package com.disney.projects.loops;

import java.util.Scanner;

public class ForBinomio {

	public static void main(String[] args) {

		ForExerciseBinomio forExerciseBinomio = new ForExerciseBinomio();

		Scanner scanner = new Scanner(System.in);

		System.out.println("How many times to repeat?");
		int repeat = scanner.nextInt();

		for (int i = 1; i < repeat; i++) {

			System.out.println("Enter value a");
			int a = scanner.nextInt();
			System.out.println("Enter value b");
			int b = scanner.nextInt();

			forExerciseBinomio.setA(a);
			forExerciseBinomio.setB(b);
			forExerciseBinomio.binomio();

			System.out.println("result square root a  " + "  " + Math.sqrt(a));
			System.out.println("result square root b" + "  " + Math.sqrt(b));
		}

		scanner.close();

	}
}
