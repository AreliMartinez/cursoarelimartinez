package com.disney.projects.loops;

import java.util.Scanner;

public class ExerciseTableMultiplication {

	public static void main(String[] args) {
		ExerciseTable exerciseTable = new ExerciseTable();

		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter number of the table multiplication");
		int table = scanner.nextInt();
		System.out.println("Enter the limit number of the table");
		int stop = scanner.nextInt();

		exerciseTable.setTable(table);
		exerciseTable.setStop(stop);
		exerciseTable.multiplicationTable();

		scanner.close();
	}

}