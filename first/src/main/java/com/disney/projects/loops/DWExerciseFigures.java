package com.disney.projects.loops;

public class DWExerciseFigures {

	private double base;
	private double height;
	private double result;
	private int option;
	private String answer;

	public DWExerciseFigures() {

	}

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;

	}

	public int getOption() {
		return option;
	}

	public void setOption(int option) {
		this.option = option;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public double getResult() {
		return result;
	}

	public void setResult(double result) {
		this.result = result;
	}

	public void figuresGeometric() {

		setResult(2 * (getBase() + getHeight()));
		switch (getOption()) {
		case 1:
			System.out.println("Perimeter of the Rectangle" + " " + result);
			break;
		case 2:
			System.out.println("Perimeter of the Parallelogram" + " " + result);
			break;
		case 3:
			System.out.println("Perimeter of the Kite" + " " + result);
			break;
		default:
			System.out.println("There is no such option");
			break;
		}
	}

}