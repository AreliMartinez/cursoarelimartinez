package com.disney.projects.loops;

import java.util.Scanner;

public class WhileQualification {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int count = 1;

		WhileExerciseQualification whileExerciseQualification = new WhileExerciseQualification();

		System.out.println("How many times should you repeat? ");
		int repeat = scanner.nextInt();
		while (count <= repeat) {

			count++;

			System.out.println("Enter note one");
			double one = scanner.nextDouble();
			System.out.println("Enter note two");
			double two = scanner.nextDouble();
			System.out.println("Enter note three");
			double three = scanner.nextDouble();

			whileExerciseQualification.setOne(one);
			whileExerciseQualification.setTwo(two);
			whileExerciseQualification.setThree(three);
			whileExerciseQualification.qualification();

		}

		scanner.close();
	}

}
