package com.disney.projects;

import java.util.Scanner;

//if-else
public class Shopping {

	public static void shopping(double quantity) {

		if (quantity >= 8000 && quantity <= 10000) {
			double percentage50 = quantity * 0.50;
			double result50 = quantity - percentage50;
			System.out.println("50% discount applies , the total is" + " " + result50);

		} else if (quantity >= 5000 && quantity < 8000) {
			double percentage30 = quantity * 0.30;
			double result30 = quantity - percentage30;
			System.out.println("30% discount applies, the total is" + " " + result30);

		} else if (quantity >= 2000 && quantity < 5000) {
			double percentage20 = quantity * 0.20;
			double result20 = quantity - percentage20;
			System.out.println("20% discount applied, the total is" + " " + result20);

		} else {
			System.out.println("Does not apply discount");

		}

	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter the amount purchased");
		double quantity = scanner.nextDouble();

		shopping(quantity);
		scanner.close();
	}

}
