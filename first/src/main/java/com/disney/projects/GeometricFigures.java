package com.disney.projects;

import java.util.Scanner;
//switch

public class GeometricFigures {

	public static void geometric(double base, double height, int option) {
		double result = 2 * (base + height);
		switch (option) {
		case 1:
			System.out.println("Perimeter of the Rectangle" + " " + result);
			break;
		case 2:
			System.out.println("Perimeter of the Parallelogram" + " " + result);
			break;
		case 3:
			System.out.println("Perimeter of the Kite" + " " + result);
			break;
		default:
			System.out.println("There is no such option");
			break;
		}
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter base");
		double base = scanner.nextDouble();
		System.out.println("Enter height");
		double height = scanner.nextDouble();

		System.out.println("Welcome ,Choose an option");
		System.out.println("1.Rectangle");
		System.out.println("2.Parallelogram");
		System.out.println("3.Kite");
		int option = scanner.nextInt();

		geometric(base, height, option);

		scanner.close();
	}

}
