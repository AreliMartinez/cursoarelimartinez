package com.disney.projects.abstracts;

public abstract class Hospital {

	private String name;
	private String hospital;

	public Hospital() {

	}

	public Hospital(String name, String hospital) {
		this.name = name;
		this.hospital = hospital;
	}

	public void medicalConsultation() {
		System.out.println("Welcome" + " " + getName());
		System.out.println("Hospital: " + " " + getHospital());

		calculateSchedule();

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHospital() {
		return hospital;
	}

	public void setSpeciality(String hospital) {
		this.hospital = hospital;
	}

	public abstract void calculateSchedule();

}
