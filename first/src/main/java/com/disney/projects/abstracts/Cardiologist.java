package com.disney.projects.abstracts;

public class Cardiologist extends Hospital {

	public static final String HOSPITAL = "National Institute of Cardiology";

	public Cardiologist(String name) {
		super(name, HOSPITAL);

	}

	public void calculateSchedule() {
		System.out.println("Speciality: Cardiologist");
		System.out.println("Dra.Maria Luisa Perez Sanchez");
		System.out.println("Professional License:09897656");
		System.out.println("Horary: 9:00 a.m - 14:00 p.m");

	}

}
