package com.disney.projects.abstracts;

import java.util.Scanner;

public class ATM {

	public static void amountWithdraw(int quantity) {
		int tickets1000 = 0, tickets500 = 0, tickets200 = 0, tickets100 = 0, tickets50 = 0, tickets20 = 0, coin10 = 0,
				coin5 = 0, coin2 = 0, coin1 = 0;

		if (quantity > 10000) {
			System.out.println("Error!!, Enter a lower amount");

		} else {

			tickets1000 = quantity / 1000;
			quantity = quantity % 1000;
			System.out.println(tickets1000 + " " + "ticket of $1000 ");

			tickets500 = quantity / 500;
			quantity = quantity % 500;
			System.out.println(tickets500 + " " + "ticket of $500 ");

			tickets200 = quantity / 200;
			quantity = quantity % 200;
			System.out.println(tickets200 + " " + "ticket of $200 ");

			tickets100 = quantity / 100;
			quantity = quantity % 100;
			System.out.println(tickets100 + " " + "ticket of $100 ");

			tickets50 = quantity / 50;
			quantity = quantity % 50;
			System.out.println(tickets50 + " " + "ticket of $50 ");

			tickets20 = quantity / 20;
			quantity = quantity % 20;
			System.out.println(tickets20 + " " + "ticket of $20 ");

			coin10 = quantity / 10;
			quantity = quantity % 10;
			System.out.println(coin10 + " " + "coins of $10 ");

			coin5 = quantity / 5;
			quantity = quantity % 5;
			System.out.println(coin5 + " " + "coins of $5 ");

			coin2 = quantity / 2;
			quantity = quantity % 2;
			System.out.println(coin2 + " " + "coins of $2 ");

			coin1 = quantity / 1;
			quantity = quantity % 1;
			System.out.println(coin1 + " " + "coins of $1 ");
		}
	}

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		String repeat = "N";

		do {

			System.out.println("What is the amount you want to withdraw?");
			int quantity = scanner.nextInt();

			amountWithdraw(quantity);

			System.out.println("Do you want to withdraw again? Y/N ");
			repeat = scanner.next();

		} while (repeat.equalsIgnoreCase("Y"));
		scanner.close();

	}

}
