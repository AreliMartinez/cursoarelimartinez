package com.disney.projects.abstracts;

public class Dermatology extends Hospital {

	public static final String HOSPITAL = "Iztapalapa Pediatric Hospital";

	public Dermatology(String name) {
		super(name, HOSPITAL);

	}

	public void calculateSchedule() {
		System.out.println("Speciality : Dermatology");
		System.out.println("Dr.Ulises Pericles Saltillo");
		System.out.println("Professional License:06845678");
		System.out.println("Horary: 15:00 p.m - 20:00 p.m");

	}

}
